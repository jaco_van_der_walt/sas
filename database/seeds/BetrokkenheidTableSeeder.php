<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class BetrokkenheidTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('betrokkenheid')->insert([
            'id' => Uuid::generate()->string,
            'betrokkenheid' => 'Ontdekker',
            'tipe' => 'Jeuglid',
        ]);

        DB::table('betrokkenheid')->insert([
            'id' => Uuid::generate()->string,
            'betrokkenheid' => 'PD',
            'tipe' => 'Jeuglid',
        ]);

        DB::table('betrokkenheid')->insert([
            'id' => Uuid::generate()->string,
            'betrokkenheid' => 'Verkenner',
            'tipe' => 'Jeuglid',
        ]);

        DB::table('betrokkenheid')->insert([
            'id' => Uuid::generate()->string,
            'betrokkenheid' => 'Staatmaker',
            'tipe' => 'Volwassene',
        ]);

        DB::table('betrokkenheid')->insert([
            'id' => Uuid::generate()->string,
            'betrokkenheid' => 'Offisier',
            'tipe' => 'Volwassene',
        ]);

        DB::table('betrokkenheid')->insert([
            'id' => Uuid::generate()->string,
            'betrokkenheid' => 'Heemraad',
            'tipe' => 'Volwassene',
        ]);

        DB::table('betrokkenheid')->insert([
            'id' => Uuid::generate()->string,
            'betrokkenheid' => 'Jeugvriend',
            'tipe' => 'Volwassene',
        ]);

    }
}
