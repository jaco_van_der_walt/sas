<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class GebiedeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('gebiede')->insert([
            'naam' => 'Transvaal',
            'id' => Uuid::generate()->string,
        ]);

        DB::table('gebiede')->insert([
            'naam' => 'Kaapland',
            'id' => Uuid::generate()->string,
        ]);

        DB::table('gebiede')->insert([
            'naam' => 'Vrystaat',
            'id' => Uuid::generate()->string,
        ]);

        DB::table('gebiede')->insert([
            'naam' => 'Namibie',
            'id' => Uuid::generate()->string,
        ]);
    }
}
