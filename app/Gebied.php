<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Gebied extends Model
{

    use Uuids;
    protected $table = 'gebiede';

    public function oorde()
    {
    	return $this->hasMany('App\Oord');
    }

    public $incrementing = false;
}
