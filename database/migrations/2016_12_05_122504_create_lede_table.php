<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLedeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lede', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('voortrekker_registrasie_nommer')->nullable();
            $table->string('noemnaam');
            $table->string('van');
            $table->date('geboortedatum');
            $table->char('geslag',1);
            $table->timestamps();

            $table->primary('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lede');
    }
}
