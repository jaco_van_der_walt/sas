<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	 $this->call(UsersTableSeeder::class);
         $this->call(GebiedeTableSeeder::class);
         $this->call(OordeTableSeeder::class);
         $this->call(KommandosTableSeeder::class);
         $this->call(LedeTableSeeder::class);
         $this->call(BetrokkenheidTableSeeder::class);

    }
}
