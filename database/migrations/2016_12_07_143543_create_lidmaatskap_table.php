<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLidmaatskapTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('lidmaatskap', function (Blueprint $table) {
            $table->uuid('kommando_jaar_id');
            $table->uuid('lid_id');
            $table->uuid('betrokkenheid_id');
        });

        Schema::table('lidmaatskap', function($table) {
           $table->foreign('kommando_jaar_id')->references('id')->on('kommando_jare');
           $table->foreign('lid_id')->references('id')->on('lede');
           $table->foreign('betrokkenheid_id')->references('id')->on('betrokkenheid');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lidmaatskap');
    }
}
