<?php

namespace App\Http\Controllers;
use App\Lid;
use Validator;
use App\Http\Resources\LidResource;

use Illuminate\Http\Request;


class LidController extends Controller
{

    //API
    public function updateLid($id, Request $request)
    {
      $validator = Validator::make($request->all(), [
            'noemnaam' => 'required',
            'van' => 'required',
            'geboortedatum' => 'required',
            'geslag' => 'required|in:M,F'
        ]);
      if($validator->passes())
      {
        $lid = Lid::find($id);
        if($lid)
        {
          $lid->noemnaam = $request->input('noemnaam');
          $lid->van = $request->input('van');
          $lid->geboortedatum = $request->input('geboortedatum');
          $lid->geslag = $request->input('geslag');

          $lid->save();

          return new LidResource($lid);
        }
        else {
          return response()->json(['error'=>'Lid not found'],204);
        }
      }
      else{
        return response()->json(['error'=>$validator->errors()->all()],400);
      }

    }

    //Frontend
    public function getLede()
    {
    	return view('lede.view_lede');
    }

    public function getLid($id)
    {
    	$lid = Lid::find($id);
    	if($lid)
    	{
    		return view('lede.view_lid', ["lid" => $lid]);
    	}
    	else
    	{
    		return "Die lid kon nie gevind word nie";
    	}
    }

    public function searchVRN(Request $request)
    {
        $q = $request->input('q');

        $results = Lid::search($q)->get();
        foreach($results as $result)
        {
            $data[] = array('id' => $result->id, 'text' => $result->noemnaam." ".$result->van, 'data' => $result);
        }

        return json_encode($data);
    }

}
