<?php

namespace App\Http\Resources;

use App\Lid;
use App\Betrokkenheid;
use Illuminate\Http\Resources\Json\Resource;

class LidResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'voortrekker_registrasie_nommer' => $this->voortrekker_registrasie_nommer,
        'noemnaam' => $this->noemnaam,
        'van' => $this->van,
        'geboortedatum' => $this->geboortedatum,
        'geslag' => $this->geslag,
        'betrokkenheid' => $this->whenPivotLoaded('lidmaatskap', function () {
            return Betrokkenheid::find($this->pivot->betrokkenheid_id)->firstOrFail()->betrokkenheid;
        }),
        'links' => [
              'self' => url('api/lede',$this->id)
          ],
      ];
    }
}
