<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\OordeResource;

class GebiedResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'naam' => $this->naam,
          'oorde' => new OordeResource($this->oorde),
          'links' => [
                'self' => url('api/gebiede',$this->id),
            ],
        ];
    }
}
