<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Betrokkenheid extends Model
{
  use Uuids;
  protected $table = 'betrokkenheid';

  public $incrementing = false;
}
