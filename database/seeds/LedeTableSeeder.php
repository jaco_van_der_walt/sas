<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class LedeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'CK1234',
            'noemnaam' => 'Jan',
            'van' => 'Pierewiet',
            'geboortedatum' => '1988-10-25',
            'geslag' => 'M',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'J1235',
            'noemnaam' => 'Sarel',
            'van' => 'Cilliers',
            'geboortedatum' => '1950-07-09',
            'geslag' => 'M',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'T54123',
            'noemnaam' => 'Tibbie',
            'van' => 'Visser',
            'geboortedatum' => '1962-11-23',
            'geslag' => 'F',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'S541554',
            'noemnaam' => 'Sannie',
            'van' => 'van der Merwe',
            'geboortedatum' => '1985-03-15',
            'geslag' => 'F',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'B1235',
            'noemnaam' => 'Bert',
            'van' => 'Venter',
            'geboortedatum' => '2000-12-12',
            'geslag' => 'M',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'L12351',
            'noemnaam' => 'Laetitia',
            'van' => 'van Vuuren',
            'geboortedatum' => '2000-08-16',
            'geslag' => 'F',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'K12356',
            'noemnaam' => 'Karel',
            'van' => 'van Rensburg',
            'geboortedatum' => '1999-08-21',
            'geslag' => 'M',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'J159223',
            'noemnaam' => 'Johan',
            'van' => 'Grobler',
            'geboortedatum' => '1955-08-26',
            'geslag' => 'M',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'K12351',
            'noemnaam' => 'Karin',
            'van' => 'van Asbeek',
            'geboortedatum' => '1988-05-16',
            'geslag' => 'F',
        ]);
        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'M12354',
            'noemnaam' => 'Marcelle',
            'van' => 'Mol',
            'geboortedatum' => '2008-05-15',
            'geslag' => 'F',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'R58961',
            'noemnaam' => 'Robert',
            'van' => 'Swartz',
            'geboortedatum' => '2006-10-05',
            'geslag' => 'M',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'P12512',
            'noemnaam' => 'Pieter',
            'van' => 'Booysen',
            'geboortedatum' => '1999-03-23',
            'geslag' => 'M',
        ]);

        DB::table('lede')->insert([
            'id' => Uuid::generate()->string,
            'voortrekker_registrasie_nommer' => 'G1594',
            'noemnaam' => 'GJ',
            'van' => 'Hoogenboezem',
            'geboortedatum' => '1972-01-23',
            'geslag' => 'M',
        ]);
    }
}
