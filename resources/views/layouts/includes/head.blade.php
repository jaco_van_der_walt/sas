@section('head')
@show
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>Die Voortrekkers - SASv2</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css" />
<link rel="apple-touch-icon" href="pages/ico/60.png">
<link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
<link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
<link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content="" name="description" />
<meta content="" name="author" />
<!-- BEGIN Vendor CSS-->
<link href="{{asset('html/assets/plugins/pace/pace-theme-flash.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('html/assets/plugins/bootstrapv3/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('html/assets/plugins/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('html/assets/plugins/jquery-scrollbar/jquery.scrollbar.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{asset('html/assets/plugins/bootstrap-select2/select2.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{asset('html/assets/plugins/switchery/css/switchery.min.css')}}" rel="stylesheet" type="text/css" media="screen" />

<link media="screen" type="text/css" rel="stylesheet" href="{{asset('html/assets/plugins/nvd3/nv.d3.min.css')}}"/>
<link href="{{asset('html/assets/plugins/rickshaw/rickshaw.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('html/assets/plugins/nvd3/nv.d3.min.css')}}" rel="stylesheet" type="text/css" media="screen" />
<link href="{{asset('html/assets/plugins/mapplic/css/mapplic.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('html/assets/plugins/rickshaw/rickshaw.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('html/assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css" media="screen">
<link href="{{asset('html/assets/plugins/jquery-metrojs/MetroJs.css')}}" rel="stylesheet" type="text/css" media="screen" />



<!-- Datatables -->
<link type="text/css" rel="stylesheet" href="{{asset('html/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('html/assets/plugins/jquery-datatable/extensions/FixedColumns/css/dataTables.fixedColumns.min.css')}}">
<link media="screen" type="text/css" rel="stylesheet" href="{{asset('html/assets/plugins/datatables-responsive/css/datatables.responsive.css')}}">
<link media="screen" type="text/css" rel="stylesheet" href="{{asset('html/assets/plugins/jquery-datatable/media/css/dataTables.bootstrap.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('html/assets/plugins/jquery-datatable/media/css/jquery.dataTables_themeroller.css')}}">
<link type="text/css" rel="stylesheet" href="{{asset('html/assets/plugins/jquery-datatable/media/css/jquery.dataTables.min.css')}}">
<!-- Datepicker -->
<link media="screen" type="text/css" rel="stylesheet" href="{{asset('html/assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}">

<!-- Select2 -->
<link media="screen" type="text/css" rel="stylesheet" href="{{asset('html/assets/plugins/bootstrap-select2/select2.css')}}">

<!-- BEGIN Pages CSS-->
<link href="{{asset('html/pages/css/pages-icons.css')}}" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="{{asset('html/pages/css/pages.css')}}" rel="stylesheet" type="text/css" />


<!--[if lte IE 9]>
    <link href="pages/css/ie9.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript">
window.onload = function()
{
  // fix for windows 8
  if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
    document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="{{asset('pages/css/windows.chrome.fix.css')}}" />'
}
</script>
