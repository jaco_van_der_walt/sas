<!-- START FOOTER -->
<div class="container-fluid container-fixed-lg footer">
  <div class="copyright sm-text-center">
    <p class="small no-margin pull-left sm-pull-reset">
      <span class="hint-text">Kopiereg © 2016</span>
      <span class="font-montserrat">Jaco van der Walt</span>.
      <span class="hint-text">Alle regte voorbehou</span>
      <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a>
                </span>
    </p>
    <p class="small no-margin pull-right sm-pull-reset">
      <span class="hint-text">૮(•̀ꐧ•́)ა</span>
    </p>
    <div class="clearfix"></div>
  </div>
</div>
<!-- END FOOTER -->