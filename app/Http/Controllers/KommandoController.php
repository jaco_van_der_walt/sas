<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Gebied;
use App\Oord;
use App\Kommando;
use App\KommandoJaar;
use App\Betrokkenheid;
use App\Lid;
use App\Lidmaatskap;
use Validator;
use App\Http\Resources\LedeResource;
use Illuminate\Support\Facades\Log;

class KommandoController extends Controller
{

    //API
    //Register a lid to a kommando for a year
    public function registreerLid(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'kommando_id' => 'required',
            'jaar' => 'required',
            'lid_id' => 'required',
            'betrokkenheid_id' => 'required'
        ]);
      if($validator->passes())
      {
        //Get and check the existence of all models
        $kommandojaar = KommandoJaar::firstOrCreate(['jaar' => $request->input('jaar'), 'kommando_id' => $request->input('kommando_id')]);
        $lid = Lid::where('id', $request->input('lid_id'))->firstOrFail();
        $betrokkenheid = Betrokkenheid::where('id', $request->input('betrokkenheid_id'))->firstOrFail();

        //Check if lidmaatskap already exists
        $lidmaatskap = Lidmaatskap::where('lid_id',$lid->id)->get();

        foreach($lidmaatskap as $l)
        {
          $k = KommandoJaar::find($l->kommando_jaar_id);
          if($k->jaar == $request->input('jaar'))
          {
            return response()->json(['error'=>'Die lid het reeds lidmaatskap vir die jaar'],400);
          };
        };

        //Attatch the lid to a kommandojaar
        $kommandojaar->lede()->attach($lid, ['betrokkenheid_id' => $betrokkenheid->id]);

        return response()->json(['success'=>'lid registered'],200);
      }
      else{
        return response()->json(['error'=>$validator->errors()->all()],400);
      }
    }

    //Get all lede in a Kommando for a Jaar
    public function getKommandoLede($kommando_id, $jaar)
    {
      $kommandoJaar = KommandoJaar::where('kommando_id', $kommando_id)->where('jaar',$jaar)->firstOrFail();
      return (new LedeResource($kommandoJaar->lede))
                    ->additional(['meta' => [
                                  'count' => $kommandoJaar->lede()->count(),
                                  'kommando_jaar' => $kommandoJaar->jaar,
                                  'kommando' => $kommandoJaar->kommando->naam,
                              ]]);
    }

    //This returns the Kommando Dashboard View
    public function getKommando($jaar, $id)
    {
    	$kommando = Kommando::find($id);
    	$oord = $kommando->oord;
    	$gebied = $oord->gebied;
      $kommandoJaar = KommandoJaar::where('kommando_id', $kommando->id)->where('jaar',$jaar)->first();
      if($kommandoJaar)
      {
        $lede_count = $kommandoJaar->lede->count();
      }
      else {
        $lede_count = 0;
      }

    	session(['kommando_jaar' => $jaar]); //Set in session so we know what year we dealing with

        if(KommandoJaar::where('kommando_id',$kommando->id)->where('jaar', $jaar)->count() > 0)
        {
            // For the PieChart
            $betrokkenheid_count_array = array();
            foreach(Betrokkenheid::all() as $b)
            {
               $count = $kommandoJaar->lede()->wherePivot('betrokkenheid_id','=', $b->id)->count();
               if($count > 0)
               {
                 $betrokkenheid_count_array[$b->betrokkenheid] = $count; //only set if value exist. Don't want zeros in Piechart
               }
            }

            //For the Line Chart
            //Side note: This is horrible and can be done better :| Should maybe iterate over fixed years
            $growth_count_array = array();
            $jaar_prev_prev = KommandoJaar::where('kommando_id',$kommando->id)->where('jaar', $jaar-2)->first();
            $jaar_prev = KommandoJaar::where('kommando_id',$kommando->id)->where('jaar', $jaar-1)->first();
            $jaar_current = KommandoJaar::where('kommando_id',$kommando->id)->where('jaar', $jaar)->first();
            $jaar_next = KommandoJaar::where('kommando_id',$kommando->id)->where('jaar', $jaar+1)->first();
            $jaar_next_next = KommandoJaar::where('kommando_id',$kommando->id)->where('jaar', $jaar+2)->first();

            if($jaar_prev_prev)
            {
              $growth_count_array[$jaar-2] = $jaar_prev_prev->lede->count();
            } else {
              $growth_count_array[$jaar-2] = null;
            };
            if($jaar_prev)
            {
              $growth_count_array[$jaar-1] = $jaar_prev->lede->count();
            } else {
              $growth_count_array[$jaar-1] = null;
            };
            if($jaar_current)
            {
              $growth_count_array[$jaar] = $jaar_current->lede->count();
            } else {
              $growth_count_array[$jaar] = null;
            };
            if($jaar_next)
            {
              $growth_count_array[$jaar+1] = $jaar_next->lede->count();
            } else {
              $growth_count_array[$jaar+1] = null;
            };
            if($jaar_next_next)
            {
              $growth_count_array[$jaar+2] = $jaar_next_next->lede->count();
            } else {
              $growth_count_array[$jaar+2] = null;
            };

            //KommandoJaar data has been created
            return view('kommando.view_kommando', ['kommando' => $kommando, 'oord' => $oord, 'gebied' =>$gebied, 'jaar' => $jaar, 'lede_count' => $lede_count, 'betrokkenheid_count_array' => $betrokkenheid_count_array, 'growth_count_array' => $growth_count_array]);
        }
        else
        {

           return view('kommando.view_kommando_no_data', ['kommando' => $kommando, 'oord' => $oord, 'gebied' =>$gebied, 'jaar' => $jaar ]);
        }


    }

    public function getKommandoGuessYear($id)
    {
    	$jaar = session('kommando_jaar');
    	if ($jaar)
    	{
    		return redirect('kommando/'.$jaar.'/'.$id);
    	}
    	else
    	{
    		$jaar = date("Y"); //current year
    		session(['kommando_jaar' => $jaar]);
    		return redirect('kommando/'.$jaar.'/'.$id);
    	}
    }

    public function createKommandoJaar($jaar, $id)
    {
        $kommandojaar = KommandoJaar::firstOrCreate(['jaar' => $jaar, 'kommando_id' => $id]);
        return redirect('kommando/'.$jaar.'/'.$id);
    }

    public function getKommandoAddLid($jaar, $id)
    {
        $kommando = Kommando::find($id);
        $oord = $kommando->oord;
        $gebied = $oord->gebied;
        $betrokkenheid = Betrokkenheid::all();
        session(['kommando_jaar' => $jaar]);
        return view('kommando.view_kommando_add_lid', ['kommando' => $kommando, 'oord' => $oord, 'gebied' =>$gebied, 'jaar' => $jaar, 'betrokkenheid'=>$betrokkenheid ]);
    }
}
