![Alt text](https://voortrekkers.co.za/wp-content/uploads/2017/06/voortrekkerwapen_naam.svg)

#Sentrale Administrasie Stelsel v2

## Aangaande SASv2

SASv2 word ontwikkel om die huidige SAS stelsel te vervang. SASv2 poog om:

- 'n maklike, vinnige stelsel te wees 
- data as *inligting* te bied
- 'n API koppelpunt vir integrasie te gee
- lededata veilig te stoor


## Ontwikkelingsproses

SASv2 word ontwikkel in verskillende fases volgens gegewe prioriteite (wat mag verander soos die stelsel ontwikkel. Ontwikkeling verskied in die volgende orde:

1. Lededatabasis **(Huidige Fase)**
2. Erkenning en Toekenning
3. Kampe/Inskrywings
4. Finansies en Betaling

## Bydrae

SASv2 is 'n oopbron-projek en lede van Die Voortrekkers wat belangstel om te ontwikkel word uitgenooi om 'n bydrae te maak. 

SASv2 is ontwikkel in [Laravel](https://laravel.com) met PHP as programeertaal. SASv2 gebruik 'n MySQL databasis.

1. Om te begin, trek die kode vanaf die BitBucket bewaarplek. Maak seker jy gebruik die **develop** tak en nie **master** nie.
2. Maak seker dat jou Laravel-omgewing opgestel is. Verwys na [die Laravel Installasie Instruksies](https://laravel.com/docs/5.5/installation)
3. Die projek se afhanklikhede word deur Composer hanteer. Gebruik `composer update` om die projek se externe pakkette af te laai. 
3. Elke Laravel Projek het 'n `.env` leêr wat die opsies vir die projek bevat. **MOET NIE DIE LEÊR IN DIE WEERGAWE BEHEERSTELSEL STOOR NIE**. 
4. Stel jou databasis konneksie in die `.env` op volgens jou sisteem se omgewing.
5. Gebruik `php artisan key:generate` om 'n nuwe *Application Key* te genereer. Die Sleutel word gebruik om enkripsie op te stel vir jou program. Moet nooit jou Application Key op meer as een stelsel gebruik nie (bv. jou ontwikkeling key verskil van die key op die produksie verskaffer).
6. Skep 'n databasis in MySQL.
7. Gebruik `php artisan migrate` om die nodige tabelle in die databasis te skep.
8. In die projek onder Database -> Seeds -> UserSeeder verander die naam, e-pos en wagwoord vir jou ontwikkelings gebruiker. Gebruik dan `php artisan db:seed` om die nodige dummy data te skep.  
9. Gebruik `php artisan serve` om die projek te bedien op jou ontwikkelingsmasjien.