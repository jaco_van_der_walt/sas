<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Oord;
use App\Gebied;
use App\Kommando;
use App\KommandoJaar;
use App\Betrokkenheid;
use Illuminate\Support\Facades\Log;

class OordController extends Controller
{
    public function getOord($id)
    {
    	$oord = Oord::find($id);
    	$gebied = $oord->gebied;
    	$kommandos = $oord->kommandos()->orderBy('naam')->get();
      $current_jaar = date('Y');

      //lidmaatskap counter
      $lidmaatskap_counter = 0;
      foreach($kommandos as $k)
      {
        $kommando_jaar = KommandoJaar::where('kommando_id',$k->id)->where('jaar', $current_jaar)->first();
        if($kommando_jaar)
        {
          $lidmaatskap_counter += $kommando_jaar->lede->count();
        }
      }

      //Growth Stats for Line Chart
      $oord_growth_array = array();
      foreach($kommandos as $k)
      {
        for ($i = $current_jaar -4; $i <= $current_jaar; $i++) {
          $kommando_jaar = KommandoJaar::where('kommando_id',$k->id)->where('jaar', $i)->first();
          if($kommando_jaar)
          {
            $oord_growth_array[$k->naam][$i] = $kommando_jaar->lede->count();
          } else {
            $oord_growth_array[$k->naam][$i] = null;
          }
        }
      }

      Log::info("Oord growth array: ".json_encode($oord_growth_array));

      //Betrokkenheid stats for Pie Chart
      $oord_betrokkenheid_array = array();
      foreach($kommandos as $k)
      {
        $kommandoJaar = KommandoJaar::where('kommando_id',$k->id)->where('jaar', $current_jaar)->first();
        if($kommandoJaar)
        {
          foreach(Betrokkenheid::all() as $b)
          {
             $count = $kommandoJaar->lede()->wherePivot('betrokkenheid_id','=', $b->id)->count();
             if($count > 0)
             {
               //Need to make sure that the key exists before trying to increment it
               if(array_key_exists($b->betrokkenheid, $oord_betrokkenheid_array))
               {
                 $oord_betrokkenheid_array[$b->betrokkenheid] = $oord_betrokkenheid_array[$b->betrokkenheid] + $count; //only set if value exist. Don't want zeros in Piechart
               } else { //Key does not exist yet so just create it for now
                 $oord_betrokkenheid_array[$b->betrokkenheid] = $count;
               }

             }
          }
        }
      }


    	return view('oord.view_oord', ['oord' => $oord, 'gebied' => $gebied, 'kommandos' => $kommandos, 'lidmaatskap_counter' => $lidmaatskap_counter, 'oord_growth_array' => $oord_growth_array, 'oord_betrokkenheid_array'=>$oord_betrokkenheid_array]);
    }
}
