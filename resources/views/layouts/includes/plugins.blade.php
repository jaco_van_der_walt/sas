<!-- BEGIN VENDOR JS -->
<script src="{{asset('html/assets/plugins/pace/pace.min.js')}}" type="text/javascript"></script>
<script src="{{asset('html/assets/plugins/jquery/jquery-1.11.1.min.js')}}" type="text/javascript"></script>
<script src="{{asset('html/assets/plugins/modernizr.custom.js')}}" type="text/javascript"></script>
<script src="{{asset('html/assets/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{asset('html/assets/plugins/bootstrapv3/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('html/assets/plugins/jquery/jquery-easy.js')}}" type="text/javascript"></script>
<script src="{{asset('html/assets/plugins/jquery-unveil/jquery.unveil.min.js')}}" type="text/javascript"></script>
<script src="{{asset('html/assets/plugins/jquery-bez/jquery.bez.min.js')}}"></script>
<script src="{{asset('html/assets/plugins/jquery-ios-list/jquery.ioslist.min.js')}}" type="text/javascript"></script>
<script src="{{asset('html/assets/plugins/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('html/assets/plugins/jquery-actual/jquery.actual.min.js')}}"></script>
<script src="{{asset('html/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js')}}"></script>
<!-- END VENDOR JS -->
<!-- BEGIN CORE TEMPLATE JS -->
<script src="{{asset('html/pages/js/pages.js')}}" type="text/javascript"></script>
<!-- END CORE TEMPLATE JS -->

<script>
$(document).ready(function() {

        //Setup Ajax
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{csrf_token()}}'
            }
        });

       // Initializes search overlay plugin.
       // Replace onSearchSubmit() and onKeyEnter() with
       // your logic to perform a search and display results
       $('[data-pages="search"]').search({
           searchField: '#overlay-search',
           closeButton: '.overlay-close',
           suggestions: '#overlay-suggestions',
           brand: '.brand',
           onSearchSubmit: function(searchString) {
               console.log("Search for: " + searchString);
           },
           onKeyEnter: function(searchString) {
               console.log("Live search for: " + searchString);
               var searchField = $('#overlay-search');
               var searchResults = $('.search-results');

               $.ajax({
                     type: 'GET',
                     url: "{{url('api/search/lede')}}" + '?q=' + searchString,
                     dataType: 'json',
                     success: function (data) {
                        $("#results_row").empty();
                         console.log("Results:",data)
                         searchResults.fadeOut("fast");
                         var i = 0;
                         var dataCount = data.data.length;
                         for (var i = 0; i < dataCount; i++) {
                           $('#results_row').append('<a href="' + "{{url('lede')}}/" + data.data[i].id + '"><div class="col-md-6"><div class=""><div class="thumbnail-wrapper d48 circular bg-success text-white inline m-t-10"><div><img width="50" height="50" src="{{asset('html/assets/img/profiles/avatar.jpg')}}" data-src="{{asset('html/assets/img/profiles/avatar.jpg')}}" data-src-retina="{{asset('html/assets/img/profiles/avatar2x.jpg')}}" alt=""></div></div><div class="p-l-10 inline p-t-5"><h5 class="m-b-5"><span class="semi-bold result-name">'+data.data[i].noemnaam + ' ' + data.data[i].van + '</span></h5><p class="hint-text">' + data.data[i].voortrekker_registrasie_nommer + '</p></div></div></div></a>');
                         };
                         searchResults.fadeIn("fast");
                     },
                     error: function (data) {
                         console.log('Error:', data);
                     }
                 });


               /*
               clearTimeout($.data(this, 'timer'));
               searchResults.fadeOut("fast");
               var wait = setTimeout(function() {
                   searchResults.find('.result-name').each(function() {
                       if (searchField.val().length != 0) {
                           $(this).html(searchField.val());
                           searchResults.fadeIn("fast");
                       }
                   });
               }, 500);
               $(this).data('timer', wait);
               */
           }
       });
   });
</script>
