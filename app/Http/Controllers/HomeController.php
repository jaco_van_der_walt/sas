<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    //Generate an API Token for the currently logged in user
    public function apitoken()
    {
        $user = Auth::user();

        $token = $user->createToken('SAS Token')->accessToken;

        return $token;
    }

    public function getSettings()
    {
      return view('settings');
    }
}
