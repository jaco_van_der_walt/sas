<?php

namespace App\Http\Controllers;

use App\Http\Resources\GebiedResource;
use Illuminate\Http\Request;
use App\Oord;
use App\Gebied;
use App\Kommando;
use App\KommandoJaar;
use App\Betrokkenheid;
use Illuminate\Support\Facades\Log;
use Validator;

class GebiedController extends Controller
{

    //*******************//
    //        API        //
    //*******************//

    //Add a new Gebied
    public function postGebiede(Request $request)
    {
      $validator = Validator::make($request->all(), [
            'naam' => 'required|unique:gebiede|max:255',
        ]);

      if($validator->passes())
      {
        $gebied = new Gebied();
        $gebied->naam = $request->input('naam');
        $gebied->save();
        return new GebiedResource($gebied);
      }
      else {
        return response()->json(['error'=>$validator->errors()->all()],400);
      }
    }

    //Delete a Gebied
    public function deleteGebiede($id)
    {
      $gebied = Gebied::find($id);
      if($gebied)
      {
        $gebied->delete();
        return response()->json(['status'=>'Gebied deleted'],200);
      }
      else {
        return response()->json(['error'=>'Gebied not found'],404);
      }
    }

    //Update a Gebied
    public function updateGebiede(Request $request, $id)
    {
      $validator = Validator::make($request->all(), [
            'naam' => 'unique:gebiede',
        ]);
      if($validator->passes())
      {
        $gebied = Gebied::find($id);
        if($gebied)
        {
          if($request->input('naam'))
          {
            $gebied->naam = $request->input('naam');
          }
          $gebied->save();

          return new GebiedResource($gebied);
        }
        else {
          return response()->json(['error'=>'Gebied not found'],404);
        }
      }
      else{
        return response()->json(['error'=>$validator->errors()->all()],400);
      }
    }

    //ESSENTIALLY NASIONAAL
	  //Returns view with all Gebiede
    public function getGebiede()
    {
    	$gebiede = Gebied::all();
      $current_jaar = date('Y');

      //Lidmaatskap Counter
      $lidmaatskap_counter = 0;
      foreach($gebiede as $g)
      {
        foreach($g->oorde as $o)
        {
          foreach($o->kommandos as $k)
          {
            $kommando_jaar = KommandoJaar::where('kommando_id',$k->id)->where('jaar', $current_jaar)->first();
            if($kommando_jaar)
            {
              $lidmaatskap_counter += $kommando_jaar->lede->count();
            }
          }
        };
      };



      //Betrokkenheid Pie Chart
      $nasionaal_betrokkenheid_array = array();
      foreach($gebiede as $g)
      {
        foreach($g->oorde as $o)
        {
          foreach($o->kommandos as $k)
          {
            $kommandoJaar = KommandoJaar::where('kommando_id',$k->id)->where('jaar', $current_jaar)->first();
            if($kommandoJaar)
            {
              foreach(Betrokkenheid::all() as $b)
              {
                 $count = $kommandoJaar->lede()->wherePivot('betrokkenheid_id','=', $b->id)->count();
                 if($count > 0)
                 {
                   //Need to make sure that the key exists before trying to increment it
                   if(array_key_exists($b->betrokkenheid, $nasionaal_betrokkenheid_array))
                   {
                     $nasionaal_betrokkenheid_array[$b->betrokkenheid] = $nasionaal_betrokkenheid_array[$b->betrokkenheid] + $count; //only set if value exist. Don't want zeros in Piechart
                   } else { //Key does not exist yet so just create it for now
                     $nasionaal_betrokkenheid_array[$b->betrokkenheid] = $count;
                   }

                 };
              };
            };
          };
        };
      }

      //Growth Line chart
      $nasionaal_growth_array = array();
      foreach($gebiede as $g)
      {
        for ($i = $current_jaar -4; $i <= $current_jaar; $i++)
        {
            $gebied_count_jaar = 0;
            foreach($g->oorde as $o)
            {
              foreach($o->kommandos as $k)
              {
                $kommando_jaar = KommandoJaar::where('kommando_id',$k->id)->where('jaar', $i)->first();
                if($kommando_jaar)
                {
                    $gebied_count_jaar += $kommando_jaar->lede->count();
                };
              }
            }
            if($gebied_count_jaar > 0)
            {
              $gebied_growth_array[$g->naam][$i] = $gebied_count_jaar;
            } else {
              $gebied_growth_array[$g->naam][$i] = null;
            };

        };
      };

        Log::info("gebiedecount: ".json_encode($gebied_growth_array));



		  return view('gebied.view_gebiede', ['gebiede' => $gebiede, 'lidmaatskap_counter'=>$lidmaatskap_counter, 'nasionaal_betrokkenheid_array'=>$nasionaal_betrokkenheid_array, 'gebied_growth_array'=>$gebied_growth_array]);
    }

    //returns the dashboard for a single gebied
    public function getGebied($id)
    {
    	$gebied = Gebied::find($id);
    	$oorde = $gebied->oorde()->orderBy('naam')->get();
      $current_jaar = date('Y');

      //Lidmaatskap Counter
      $lidmaatskap_counter = 0;
      foreach($oorde as $o)
      {
        foreach($o->kommandos as $k)
        {
          $kommando_jaar = KommandoJaar::where('kommando_id',$k->id)->where('jaar', $current_jaar)->first();
          if($kommando_jaar)
          {
            $lidmaatskap_counter += $kommando_jaar->lede->count();
          }
        }
      };

      //Growth Line chart
      $gebied_growth_array = array();
      foreach($oorde as $o)
      {
          for ($i = $current_jaar -4; $i <= $current_jaar; $i++)
            {
                $oord_count = 0;
                foreach($o->kommandos as $k)
                  {
                      $kommando_jaar = KommandoJaar::where('kommando_id',$k->id)->where('jaar', $i)->first();
                      if($kommando_jaar)
                      {
                          $oord_count += $kommando_jaar->lede->count();
                      };
                  };
                if($oord_count > 0)
                {
                  $gebied_growth_array[$o->naam][$i] = $oord_count;
                } else {
                  $gebied_growth_array[$o->naam][$i] = null;
                };
            };
      };


      //Betrokkenheid Pie Chart
      $gebied_betrokkenheid_array = array();
      foreach($oorde as $o)
      {
        foreach($o->kommandos as $k)
        {
          $kommandoJaar = KommandoJaar::where('kommando_id',$k->id)->where('jaar', $current_jaar)->first();
          if($kommandoJaar)
          {
            foreach(Betrokkenheid::all() as $b)
            {
               $count = $kommandoJaar->lede()->wherePivot('betrokkenheid_id','=', $b->id)->count();
               if($count > 0)
               {
                 //Need to make sure that the key exists before trying to increment it
                 if(array_key_exists($b->betrokkenheid, $gebied_betrokkenheid_array))
                 {
                   $gebied_betrokkenheid_array[$b->betrokkenheid] = $gebied_betrokkenheid_array[$b->betrokkenheid] + $count; //only set if value exist. Don't want zeros in Piechart
                 } else { //Key does not exist yet so just create it for now
                   $gebied_betrokkenheid_array[$b->betrokkenheid] = $count;
                 }

               };
            };
          };
        };
      };

    	return view('gebied.view_gebied', ['oorde' => $oorde, 'gebied' => $gebied, 'lidmaatskap_counter'=>$lidmaatskap_counter, 'gebied_betrokkenheid_array' => $gebied_betrokkenheid_array, 'gebied_growth_array'=>$gebied_growth_array]);
    }

}
