@extends('layouts.main') @section('body') @parent <!-- START PAGE CONTENT -->
<div class="content">
  <!-- START JUMBOTRON -->
  <div class="jumbotron" data-pages="parallax">
    <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
      <div class="inner">
        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
          <li>
            <p><a href="{{url('gebiede')}}">Die Voortrekkers</a></p>
          </li>
          <li>
            <a href="{{url('gebied',[$gebied->id])}}">{{$gebied->naam}}</a>
          </li>
          <li>
            <a class="active" href="{{url('oord',[$oord->id])}}">{{$oord->naam}}</a>
          </li>
        </ul><!-- END BREADCRUMB -->
      </div>
    </div>
  </div><!-- END JUMBOTRON -->
  <!-- START CONTAINER FLUID -->
  <div class="container-fluid container-fixed-lg">
    <!-- BEGIN PlACE PAGE CONTENT HERE -->
    <div class="panel-body">
    <div class="row">

        <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">Lede vir {{date("Y")}}
                </div>
            </div>
            <div class="panel-body">
              <h3><span class="semi-bold">{{$lidmaatskap_counter}}</span></h3>
            </div>
        </div>
      </div>

      <div class="col-md-3">
          <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">Betrokkenheid vir {{date("Y")}}
                </div>
            </div>
            <div class="panel-body">
              <canvas id="myChart"></canvas>
            </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="panel panel-default">
          <div class="panel-heading">
              <div class="panel-title">Groei
              </div>
          </div>
          <div class="panel-body">
            <canvas id="myChartLine"></canvas>
          </div>
      </div>
    </div>

    </div>
    <hr>
    <div class="row">
      @foreach($kommandos as $kommando)
      <a href="{{url('kommando',[$kommando->id])}}">
      <div class="col-md-4 col-lg-3 col-xlg-2">
        <div class="row">
          <div class="col-md-12 m-b-10">
            <div class="widget-8 panel no-border bg-success-dark no-margin widget-loader-bar">
              <div class="container-xs-height full-height">
                <div class="row-xs-height">
                  <div class="col-xs-height col-top">
                    <div class="panel-heading top-left top-right">
                      <div class="panel-title text-black hint-text">
                        <span class="font-montserrat fs-11 all-caps">Kommando</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row-xs-height">
                  <div class="col-xs-height col-top relative">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="p-l-20">
                          <h3 class="no-margin p-b-5 text-white">{{$kommando->naam}}</h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        </a>
      </div>@endforeach
      </div>
    </div><!-- END PLACE PAGE CONTENT HERE -->
  </div><!-- END CONTAINER FLUID -->
</div><!-- END PAGE CONTENT -->
@endsection

@section('plugins')
@parent

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/randomcolor/0.5.2/randomColor.js" type="text/javascript"></script>

<!-- Line Chart -->
<script>
$(document).ready(function(){

    //We put the blade complete JSON object here, then use JS to manipulate the graph
    var json_data = JSON.parse('{!!json_encode($oord_growth_array)!!}');
    var keys = Object.keys(json_data);
    var current_year = (new Date()).getFullYear();

    var ctx = document.getElementById('myChartLine').getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: [current_year-4, current_year-3, current_year-2, current_year-1, current_year],
      }
    });

    //Iterate through JSON_DATA and push new graphs
    for (var key in json_data) {
        if (json_data.hasOwnProperty(key)) {
          var color = randomColor({
                           luminosity: 'bright',
                           format: 'rgb'
                        });
            myChart.data.datasets.push({
              label: key,
              data: [json_data[key][current_year-4],json_data[key][current_year-3],json_data[key][current_year-2],json_data[key][current_year-1],json_data[key][current_year]],
              borderColor : color,
              backgroundColor: color,
              fill: false
            });
            myChart.update();

        }
    }
});
</script>

<!-- PieChart -->
<script>
$(document).ready(function(){
    //Do some acrobatics with the incoming betrokkenheid_count_array to split it into keys and values for the piechart.
    var json_data = JSON.parse('{!!json_encode($oord_betrokkenheid_array)!!}');
    var values = Object.keys(json_data).map(function (key) { return json_data[key]; });
    var keys = Object.keys(json_data);

    var color = randomColor({
                     luminosity: 'bright',
                     format: 'rgb',
                     count: keys.length
                  });

    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'pie',

      // The data for our dataset
      data: {
        datasets: [{
            data: values,
            backgroundColor: color,
            borderColor: color,
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: keys,
    },

      // Configuration options go here
      options: {}
  });
});
</script>
@endsection
