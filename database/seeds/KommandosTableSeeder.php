<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;
use App\Oord;

class KommandosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kommandos')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Christiaan de Wet',
            'oord_id' => Oord::where('naam','Pionier')->firstOrFail()->id,
        ]);

        DB::table('kommandos')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Dirkie Uys',
            'oord_id' => Oord::where('naam','Pionier')->firstOrFail()->id,
        ]);

        DB::table('kommandos')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Touleier',
            'oord_id' => Oord::where('naam','Pionier')->firstOrFail()->id,
        ]);

        DB::table('kommandos')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Kruithoring',
            'oord_id' => Oord::where('naam','Pionier')->firstOrFail()->id,
        ]);

        DB::table('kommandos')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Soetdoring',
            'oord_id' => Oord::where('naam','Pionier')->firstOrFail()->id,
        ]);

        DB::table('kommandos')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Suiderkruis',
            'oord_id' => Oord::where('naam','Pionier')->firstOrFail()->id,
        ]);
    }
}
