@extends('layouts.main')

@section('body')
@parent

  <!-- START PAGE CONTENT -->
  <div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
      <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
        <div class="inner">
          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
          <li>
            <p><a href="{{url('gebiede')}}">Die Voortrekkers</a></p>
          </li>
          <li>
            <a href="{{url('gebied',[$gebied->id])}}">{{$gebied->naam}}</a>
          </li>
          <li>
            <a href="{{url('oord',[$oord->id])}}">{{$oord->naam}}</a>
          </li>
          <li>
            <a class="active" href="{{url('kommando',[$kommando->id])}}">{{$kommando->naam}}</a>
          </li>
        </ul><!-- END BREADCRUMB -->
        </div>
      </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
      <!-- BEGIN PlACE PAGE CONTENT HERE -->

      <div class="panel-body">
        <!-- YEAR SELECTOR -->
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-4">
            <h3>{{$kommando->naam}} - Nuwe Lidmaatskap</h3>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-8">
              <div class="date-container">
                <div class="date-center pull-right"><h3>{{$jaar}}</h3></div>
              </div>
          </div>
          </div>
          <!-- END YEAR SELECTOR -->

          <div class="panel">





            <div id="myFormWizard">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-tabs-linetriangle nav-tabs-separator">
                    <li class="active">
                        <a data-toggle="tab" href="#tab1"><i class="fa fa-search tab-icon"></i> <span>Soek</span></a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#tab2"><i class="fa fa-sitemap tab-icon"></i> <span>Betrokkenheid</span></a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#tab3"><i class="fa fa-user tab-icon"></i> <span>Persoonlike Inligting</span></a>
                    </li>
                    <li class="">
                        <a data-toggle="tab" href="#tab4"><i class="fa fa-check tab-icon"></i> <span>Registreer</span></a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane slide-left active" id="tab1">
                        <div class="row row-same-height">
                          <div class="col-md-5 b-r b-dashed b-grey ">
                            <div class="padding-30 m-t-50">
                              <h2>Soek die persoon wat jy wil registreer</h2>
                              <p class="small hint-text">Gebruik die persoon se Voortrekker Registrasie Nommer</p>
                              <form role="form">
                                    <div class="form-group">
                                      <!-- Element intended to use with advance options -->
                                      <input type="hidden" id="mySelect2" name="mySelect2" class="full-width">
                                  </div>
                              </form>
                            </div>
                          </div>

                          <div class="col-md-7" id="lid_detail_pane" hidden>
                              <h1 id="naam_van_header">Naam & Van</h3>
                              <h3 id="vrn_header">VRN</h4>
                              <br>
                              <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label display-label">Geboortedatum</label>
                                    <div class="col-sm-9" id="lid_geboortedatum">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label display-label">Geslag</label>
                                    <div class="col-sm-9" id="lid_geslag">
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="tab-pane slide-left" id="tab2">
                      <form>
                        <div class="row row-same-height">
                          <div class="col-md-5 b-r b-dashed b-grey ">
                            <div class="padding-30 m-t-50">
                              <h2 id="tab_2_header"></h2>
                            </div>
                          </div>

                          <div class="col-md-7 padding-30 m-t-50">
                              <br>
                              <select class="cs-select cs-skin-slide" data-init-plugin="cs-select" id="betrokkenheid_select">
                                @foreach($betrokkenheid as $b)
                                <option value="{{$b->id}}">{{$b->betrokkenheid}}</option>
                                @endforeach
                              </select>
                        </div>
                      </div>
                    </form>
                    </div>

                    <div class="tab-pane slide-left" id="tab3">

                      <div class="row row-same-height">
                        <div class="col-md-5 b-r b-dashed b-grey ">
                          <div class="padding-30 m-t-50">
                            <h2 id="tab_3_header"></h2>
                            <p>Maak asseblief seker dat alle inligting op datum is</p>
                          </div>
                        </div>
                        <div class="col-md-7">
                          <form>
                          <div class="padding-30">
                              <p>Persoonlike Besonderhede</p>
                              <div class="form-group-attached">
                                <div class="row clearfix">
                                  <div class="col-sm-6">
                                    <div class="form-group form-group-default required">
                                      <label>Noemnaam</label>
                                      <input id="input_noemnaam" type="text" class="form-control" required>
                                    </div>
                                  </div>
                                  <div class="col-sm-6">
                                    <div class="form-group form-group-default required">
                                      <label>Van</label>
                                      <input id="input_van" type="text" class="form-control" required>
                                    </div>
                                  </div>
                                </div>
                                <div class="row clearfix">
                                  <div class="col-sm-12">
                                    <div class="form-group form-group-default required">
                                      <label>Geboortedatum <span class="help">jjjj-mm-dd</span></label>
                                      <input type="text" id="input_geboortedatum" class="form-control" required>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <br>
                              <div class="row clearfix">
                                <label>Geslag</label>
                                <div class="radio radio-success">
                                  <input type="radio" value="M" name="geslag" id="M">
                                  <label for="M">Manlik</label>
                                  <input type="radio" value="F" name="geslag" id="F">
                                  <label for="F">Vroulik</label>
                                </div>
                              </div>
                          </div>
                        </form>
                        </div>
                      </div>




                    </div>
                    <div class="tab-pane slide-left" id="tab4">
                      <div class="row row-same-height">
                        <div class="col-md-5 b-r b-dashed b-grey ">
                          <div class="padding-30 m-t-50">
                            <h2 id="tab_4_header"></h2>
                          </div>
                        </div>

                        <div class="col-md-7" id="tab_4_recap">
                            <h1 id="tab_4_naam_van_header"></h3>
                            <h3 id="tab_4_vrn_header"></h4>
                            <br>
                            <div class="form-horizontal">
                              <div class="form-group">
                                  <label class="col-sm-3 control-label display-label">Kommando</label>
                                  <div class="col-sm-9">
                                    {{$kommando->naam}}
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-sm-3 control-label display-label">Jaar</label>
                                  <div class="col-sm-9">
                                    {{$jaar}}
                                  </div>
                              </div>
                              <div class="form-group">
                                  <label class="col-sm-3 control-label display-label">Betrokkenheid</label>
                                  <div class="col-sm-9" id="tab_4_betrokkenheid">
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-7 slide-left" id="tab_4_registreer">
                        <div class="padding-30 m-t-50">
                          <div class="progress-circle-indeterminate" data-color="primary"></div>
                          <p id="ajax_status" style="text-align: center">Opdateer databasis</p>
                        </div>
                          <br>
                      </div>

                      <div class="col-md-7 slide-left" id="tab_4_error">
                        <div class="padding-30 m-t-50" style="text-align: center;">
                          <i class="fa fa-times fa-5x text-danger"></i>
                          <h5>Registrasie het nie voltooi nie</h5>
                          <p id="tab_4_error_message" style="text-align: centre"></p>
                        </div>
                          <br>
                      </div>

                      <div class="col-md-7 slide-left" id="tab_4_success">
                        <div class="padding-30 m-t-50" style="text-align: center;">
                          <i class="fa fa-check fa-5x text-success"></i>
                        </div>
                          <br>
                      </div>

                    </div>
                    </div>

                    <ul class="pager wizard">
                        <li class="next">
                            <button class="btn btn-primary btn-cons btn-animated from-left fa fa-chevron-right pull-right" type="button">
                                <span>Volgende</span>
                            </button>
                        </li>
                        <li class="next finish" style="display:none;">
                            <button class="btn btn-primary btn-cons btn-animated from-left fa fa-cog pull-right" type="button" id="registreer">
                                <span>Registreer</span>
                            </button>
                        </li>
                        <li class="previous first" style="display:none;">
                            <button class="btn btn-white btn-cons btn-animated from-left fa fa-cog pull-right" type="button">
                                <span>First</span>
                            </button>
                        </li>
                        <li class="previous">
                            <button class="btn btn-white btn-cons pull-right" type="button">
                                <span>Terug</span>
                            </button>
                        </li>
                    </ul>

                </div>
            </div>
          </div>
        </div>


      <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->



@endsection

@section('plugins')
@parent
<!-- Page level scripts -->
<script type="text/javascript" src="{{asset('html/assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/bootstrap-select2/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/classie/classie.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/jquery-inputmask/jquery.inputmask.min.js')}}"></script>

<script>
$(document).ready(function() {

    var persoon_id;
    var betrokkenheid;

    //Setup Ajax with CSRF token
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{csrf_token()}}'
        }
    });

    $("#input_geboortedatum").mask("9999-99-99");
    $('#tab_4_registreer').hide();
    $('#tab_4_error').hide();
    $('#tab_4_success').hide();

    //Form Wizard
    $('#myFormWizard').bootstrapWizard({
        onTabShow: function(tab, navigation, index) {


            var $total = navigation.find('li').length;
            var $current = index + 1;



            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $('#myFormWizard').find('.pager .next').hide();
                $('#myFormWizard').find('.pager .finish').show();
                $('#myFormWizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#myFormWizard').find('.pager .next').hide();
                $('#myFormWizard').find('.pager .finish').hide();
            }

            var li = navigation.find('li.active');

            var btnNext = $('#myFormWizard').find('.pager .next').find('button');
            var btnPrev = $('#myFormWizard').find('.pager .previous').find('button');

            // remove fontAwesome icon classes
            function removeIcons(btn) {
                btn.removeClass(function(index, css) {
                    return (css.match(/(^|\s)fa-\S+/g) || []).join(' ');
                });
            }

            if ($current > 1 && $current < $total) {
                var nextIcon = li.next().find('.fa');
                var nextIconClass = nextIcon.attr('class').match(/fa-[\w-]*/).join();

                removeIcons(btnNext);
                btnNext.addClass(nextIconClass + ' btn-animated from-left fa');

                var prevIcon = li.prev().find('.fa');
                var prevIconClass = prevIcon.attr('class').match(/fa-[\w-]*/).join();

                removeIcons(btnPrev);
                btnPrev.addClass(prevIconClass + ' btn-animated from-left fa');
            } else if ($current == 1) {
                // remove classes needed for button animations from previous button
                btnPrev.removeClass('btn-animated from-left fa');
                removeIcons(btnPrev);
            } else {
                // remove classes needed for button animations from next button
                btnNext.removeClass('btn-animated from-left fa');
                removeIcons(btnNext);
            }

            //If a Persoon has been set
            if((persoon_id != null) && ($current == 1))
            {
              $('#myFormWizard').find('.pager .next').show();
              $('#myFormWizard').find('.pager .finish').hide();
            };

            if($current == 2)
            {
              $('#myFormWizard').find('.pager .next').hide();
              $('#myFormWizard').find('.pager .finish').hide();
            };

            //If second Tab and a betrokkenheid has been set
            if(($current == 2) && (betrokkenheid != null))
            {
              $('#myFormWizard').find('.pager .next').show();
              $('#myFormWizard').find('.pager .finish').hide();
            };

            if($current == 3)
            {
              $('#myFormWizard').find('.pager .next').show();
              $('#myFormWizard').find('.pager .finish').hide();
            }

            if($current == 4)
            {
              //Tab 4 Headings
              $('#tab_4_header').html("Is jy gereed om "+$('#input_noemnaam').val()+" te registreer vir {{$jaar}}?");
              $('#tab_4_naam_van_header').html($('#input_noemnaam').val() + " " + $('#input_van').val());
              $('#tab_4_betrokkenheid').html($( "#betrokkenheid_select option:selected" ).text());

              $('#tab_4_registreer').hide();
              $('#tab_4_error').hide();
              $('#tab_4_success').hide();
            }

        }
    });

    //Select2
      $("#mySelect2").select2({
      ajax: {
        url: "{{url('api/search/lede')}}",
        type: "GET",
        dataType: 'json',
        delay: 250,
        data: function (params) {
          return {
            q: params,
            page: params.page
          };
        },

        results: function (data) {
              return {
                  results: $.map(data, function (data) {
                    // Do some acrobatics to pass the returned AJAX call into Select2 Data Format
                    var arrayLength = data.length;
                    var master_results = [];
                    for (var i = 0; i < arrayLength; i++) {
                        var new_array = {
                          "id": data[i].id,
                          "text": data[i].noemnaam + " " + data[i].van + " (" + data[i].voortrekker_registrasie_nommer + ")",
                        };
                        master_results.push(new_array);
                    }
                    return master_results;
                  })
              };
          },


        cache: false
    },
    minimumInputLength: 3,
  });

  //This will trigger when the Betrokkenheid is changed
  $( "#betrokkenheid_select" ).change(function() {
    betrokkenheid = $( "#betrokkenheid_select" ).val();
    $('#myFormWizard').find('.pager .next').show();
    $('#myFormWizard').find('.pager .finish').hide();
  });

  $("#input_geslag").change(function(){
    console.log("Geslag changed to: " + $("#input_geslag").val());
  });

  $('#mySelect2').on("select2-selecting", function(e) {
    //Timeoute required because DOM is still being changed by Select2
    setTimeout(function(){
      var select_val = $(e.currentTarget);
      //console.log($('#mySelect2').select2('data'));

      $.ajax({
            type: 'GET',
            url: 'http://127.0.0.1:8000/api/lede/' + $('#mySelect2').select2('data').id,
            dataType: 'json',
            success: function (data) {
                $('#naam_van_header').html(data.data.noemnaam + " " + data.data.van);
                $('#vrn_header').html(data.data.voortrekker_registrasie_nommer);
                $('#lid_geboortedatum').html(data.data.geboortedatum);
                if(data.data.geslag == 'M')
                {
                  $('#lid_geslag').html("Manlik");
                }
                else
                {
                  $('#lid_geslag').html("Vroulik");
                };
                persoon_id = data.data.id;
                $('#myFormWizard').find('.pager .next').show();
                $('#myFormWizard').find('.pager .finish').hide();
                $('#lid_detail_pane').show();
                $('#tab_2_header').html("Hoe is "+data.data.noemnaam+" betrokke by {{$kommando->naam}} in {{$jaar}}?")
                $('#tab_3_header').html("Is "+data.data.noemnaam+" se inligting korrek?");

                //Setting Tab 4's VRN here because it cannot be changed in the Form
                $('#tab_4_vrn_header').html(data.data.voortrekker_registrasie_nommer);

                //Set update fields
                $('#input_noemnaam').val(data.data.noemnaam);
                $('#input_van').val(data.data.van);

                $('#input_geboortedatum').val(data.data.geboortedatum);
                if(data.data.geslag == "M")
                {
                  $("#M").attr('checked', 'checked');
                } else {
                  $("#F").attr('checked', 'checked');
                }

            },
            error: function (data) {
                console.log('Error:', data);
            }
        });


    }, 2);

  });

  //This is run when the final "Register" button is clicked
  $('#registreer').click(function(e){
    e.preventDefault();
    //Set some headers and hide/show some stuff.
    $("#tab_4_recap").hide();
    $('#myFormWizard').find('.pager .finish').hide();
    $('#myFormWizard').find('.pager .previous').hide();

    $('#tab_4_header').html("Hou vas terwyl ons "+$('#input_noemnaam').val()+ " registreer");
    $("#tab_4_registreer").show();

    // *** First Ajax Call - Update the Lid details *** ///
    // Note: we cannot run the Ajax calls in sync; we don't want to register a lid if the update call failed. We'd rather abort and deal with the fail first.
    // Because of this, we have to do some shenanigans with putting ajax calls in functions and chaining the next one on .done or dealing with the problem on a .fail

    //Create the data object to be sent to the server
    var dataObject = { 'noemnaam': $('#input_noemnaam').val(),
                       'van': $('#input_van').val(),
                       'geboortedatum': $('#input_geboortedatum').val(),
                       'geslag': $('input[name=geslag]:checked').val(),
                       '_method':'put'};

    //This is the Ajax Call that will be made
     function updateLidAjax(){
       return $.ajax({
             type: 'PUT',
             url: 'http://127.0.0.1:8000/api/lede/' + $('#mySelect2').select2('data').id,
             dataType: 'json',
             data: dataObject,
             global: false,
             success: function (data) {
                  return data;
             },
             error: function (data) {
                 return data;
             }
         });
     }

     //This function will run after the main call is successfuly
     function afterUpdateLidAjax(data)
     {
       setTimeout(function(){
         $('#ajax_status').html("Registreer Lid");
         registreerLidAjax().done(allSuccess).fail(error);
       }, 1000);


     };

     function error(data)
     {
       setTimeout(function(){
         $('#tab_4_registreer').hide();
         $('#tab_4_error_message').html("<em>"+data.responseJSON.error+"</em>");
         $('#tab_4_error').show();
       }, 1000);
     }

     function allSuccess(data)
     {
       setTimeout(function(){
         $('#tab_4_header').html($('#input_noemnaam').val()+ " is geregistreer by {{$kommando->naam}} vir {{$jaar}}");
         $('#tab_4_registreer').hide();
         $('#tab_4_error').hide();
         $('#tab_4_success').show();
       },1000);
     }

     //Let's fire the first function and deal with the success and fails after it is complete.
     updateLidAjax().done(afterUpdateLidAjax).fail(error);

     var registreerObject = { 'kommando_id': "{{$kommando->id}}",
                        'jaar': "{{$jaar}}",
                        'lid_id': persoon_id,
                        'betrokkenheid_id': betrokkenheid,
                        '_method':'post'};

     function registreerLidAjax(){
       return $.ajax({
             type: 'POST',
             url: 'http://127.0.0.1:8000/api/kommando/lidmaatskap',
             dataType: 'json',
             data: registreerObject,
             global: false,
             success: function (data) {
                  console.log("Registrasie suksesvol: " + data);
                  return data;
             },
             error: function (data) {
                 console.log('Error:', data);
                 return data;
             }
         });
     }

  });

});
</script>
@endsection
