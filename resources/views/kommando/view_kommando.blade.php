@extends('layouts.main')

@section('body')
@parent

  <!-- START PAGE CONTENT -->
  <div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
      <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
        <div class="inner">
          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
          <li>
            <p><a href="{{url('gebiede')}}">Die Voortrekkers</a></p>
          </li>
          <li>
            <a href="{{url('gebied',[$gebied->id])}}">{{$gebied->naam}}</a>
          </li>
          <li>
            <a href="{{url('oord',[$oord->id])}}">{{$oord->naam}}</a>
          </li>
          <li>
            <a class="active" href="{{url('kommando',[$kommando->id])}}">{{$kommando->naam}}</a>
          </li>
        </ul><!-- END BREADCRUMB -->
        </div>
      </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
      <!-- BEGIN PlACE PAGE CONTENT HERE -->

      <div class="panel-body">
        <!-- YEAR SELECTOR -->
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-4">
            <h3>{{$kommando->naam}}</h3>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-8">
              <div class="date-container">
                <div class="date-left pull-right"><a href="{{url('kommando',[$jaar +1, $kommando->id])}}"><h4><i class="pg-arrow_lright_line_alt"></i></h4></a></div>
                <div class="date-center pull-right"><a data-toggle="modal" data-target="#myModal"><h3>{{$jaar}}</h3></a></div>
                <div class="date-right pull-right"><a href="{{url('kommando',[$jaar -1, $kommando->id])}}"><h4><i class="pg-arrow_left_line_alt"></i></h4></a></div>
                <div class="date-clear"></div>
              </div>
          </div>
          </div>
          <!-- END YEAR SELECTOR -->

          <div class="panel">
            <ul class="nav nav-tabs nav-tabs-simple">
              <li class="active">
                <a data-toggle="tab" href="#tab2hellowWorld">Tuisblad</a>
              </li>
              <li>
                <a data-toggle="tab" href="#tab2FollowUs">Lede</a>
              </li>
              <li>
                <a data-toggle="tab" href="#tab2Inspire">Instellings</a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab2hellowWorld">
                <div class="row">
                  <div class="col-md-3">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                          <div class="panel-title">Lede vir {{$jaar}}
                          </div>
                      </div>
                      <div class="panel-body">
                        <h3><span class="semi-bold">{{$lede_count}}</span></h3>
                      </div>
                  </div>
                </div>

                <div class="col-md-3">
                  <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title">Betrokkenheid vir {{$jaar}}
                        </div>
                    </div>
                    <div class="panel-body">
                      <canvas id="myChart"></canvas>
                    </div>
                </div>
              </div>

              <div class="col-md-6">
                <div class="panel panel-default">
                  <div class="panel-heading">
                      <div class="panel-title">Groei
                      </div>
                  </div>
                  <div class="panel-body">
                    <canvas id="myChartLine"></canvas>
                  </div>
              </div>
            </div>




                </div>
              </div>
              <div class="tab-pane" id="tab2FollowUs">
                <div class="row row-eq-height">
                  <div class="col-md-2 col-sm-4">
                    <h4>
                      Lede <span class="semi-bold">{{$jaar}}</span>
                    </h4>
                  </div>
                  <div>
                  </div>
                  <div class="col-md-10 col-sm-8">
                      <a href="{{url('kommando/lidmaatskap/nuut', [$jaar, $kommando->id])}}" class="btn btn-success btn-cons pull-right col-center" type="button">Nuwe Lidmaatskap</a>
                  </div>

                </div>

                <div class="row">
                  <div class="col-md-12">
                  <table id="tableWithSearch" class="table table-hover" cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>ID</th>
                              <th>VRN</th>
                              <th>Noemnaam</th>
                              <th>Van</th>
                              <th>Geboortedatum</th>
                              <th>Geslag</th>
                          </tr>
                      </thead>
                  </table>
                </div>
                </div>

              </div>
              <div class="tab-pane" id="tab2Inspire">
                <div class="row">
                  <div class="col-md-12">
                    <h3>
                      Follow us &amp; get updated!
                    </h3>
                    <p>
                      Instantly connect to what's most important to you. Follow your
                      friends, experts, favorite celebrities, and breaking news.
                    </p><br>
                  </div>
                </div>
              </div>
            </div>
          </div>











        </div>


      <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->


  <!-- MODALS -->
  <!-- MODAL STICK UP  -->
<div class="modal fade stick-up" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="pg-close fs-14"></i>
                </button>
                <h5>Kommando <span class="semi-bold">Jaar</span></h5>
                <p>Kies die jaar wat vertoon moet word</p>
            </div>
            <div class="modal-body">
                <form class="form-default" role="form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div id="jaar_picker" class="input-group date">
                                    <input id="date_holder" type="text" class="form-control">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-3">
                        <button type="button" id="jaar_button" class="btn btn-primary btn-lg btn-large btn-block" data-dismiss="modal">
                        Opdateer
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- END MODAL STICK UP  -->




@endsection

@section('plugins')
@parent
<!-- Page level scripts -->
<script type="text/javascript" src="{{asset('html/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}"></script>
<script src="{{asset('html/assets/plugins/datatables-responsive/js/datatables.responsive.js')}}" type="text/javascript"></script>
<script src="{{asset('html/assets/plugins/datatables-responsive/js/lodash.min.js')}}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/randomcolor/0.5.2/randomColor.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    $("#jaar_picker").datepicker({
    format: "yyyy", // Notice the Extra space at the beginning
    viewMode: "years",
    minViewMode: "years"
  });

  $( "#jaar_button" ).click(function() {
    //change the year
    var url = "{{url('kommando')}}" + "/" + $("#date_holder").val() + "/{{$kommando->id}}";
    window.location.href = url;
  });


});
</script>

<!-- PieChart -->
<script>
$(document).ready(function(){
    //Do some acrobatics with the incoming betrokkenheid_count_array to split it into keys and values for the piechart.
    var json_data = JSON.parse('{!!json_encode($betrokkenheid_count_array)!!}');
    var values = Object.keys(json_data).map(function (key) { return json_data[key]; });
    var keys = Object.keys(json_data);

    var color = randomColor({
                     luminosity: 'bright',
                     format: 'rgb',
                     count: keys.length
                  });

    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'pie',

      // The data for our dataset
      data: {
        datasets: [{
            data: values,
            backgroundColor: color,
            borderColor: color,
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: keys,
    },

      // Configuration options go here
      options: {}
  });
});
</script>

<!-- Line Chart -->
<script>
$(document).ready(function(){
    //Do some acrobatics with the incoming betrokkenheid_count_array to split it into keys and values for the piechart.
    var json_data = JSON.parse('{!!json_encode($growth_count_array)!!}');
    var values = Object.keys(json_data).map(function (key) { return json_data[key]; });
    var keys = Object.keys(json_data);

    var ctx = document.getElementById('myChartLine').getContext('2d');
    var chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'line',

      // The data for our dataset
      data: {
        datasets: [{
            label: "Lede",
            data: values,
            fill: false,
            backgroundColor: [
                'rgba(255,99,132,1)'
            ],
            borderColor: [
                'rgba(255,99,132,1)'
            ],
        }],

        // These labels appear in the legend and in the tooltips when hovering different arcs
        labels: keys,
    },

      // Configuration options go here
      options: {}
  });
});
</script>




<script>
$(document).ready(function() {
    $(function() {
      //datatables/kommando/{id}/jaar/{jaar}/lede
      $url = "{{url('/')}}" + "/datatables/kommando/{{$kommando->id}}/jaar/{{$jaar}}/lede";
      console.log($url);
      $('#tableWithSearch').DataTable({
          processing: true,
          serverSide: true,
          ajax: $url,
          language: {
            processing: "Verwerking...",
            info: "Wys _START_ tot _END_ van _TOTAL_ opgawes",
            lengthMenu:    "Wys _MENU_ opgawes",
            search: "Soek:",
            paginate: {
              first: "Eerste",
              previous: "Vorige",
              next: "Volgende",
              last: "Laaste"
            },
          },
          columns: [
              { data: 'id', name: 'id', visible: false},
              { data: 'voortrekker_registrasie_nommer', name: 'voortrekker_registrasie_nommer' },
              { data: 'noemnaam', name: 'noemnaam' },
              { data: 'van', name: 'van' },
              { data: 'geboortedatum', name: 'geboortedatum' },
              { data: 'geslag', name: 'geslag'},
          ]
      });

      //Fix the styling
      $('.dataTables_filter input').addClass('form-control');
      var table = $('#tableWithSearch').DataTable();

      // On Row click, navigate to Lid page
      $('#tableWithSearch tbody').on('click', 'tr', function () {
          var data = table.row( this ).data();
          var url = "{{url('lede')}}" + "/" + data.id;
          window.location.href = url;
      } );

      });
});
</script>



@endsection
