<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => Uuid::generate()->string,
            'name' => 'Jaco van der Walt',
            'email' => 'jaco.vn.dr.walt@gmail.com',
            'password' => bcrypt('Pa$$word1'),
        ]);
    }
}
