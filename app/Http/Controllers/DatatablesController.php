<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;

use App\Lid;
use App\KommandoJaar;

class DatatablesController extends Controller
{
    public function ledeData()
    {
        return Datatables::of(Lid::query())->make(true);
    }

    public function kommandoLedeData($id, $jaar)
    {
      $kommandoJaar = KommandoJaar::where('kommando_id', $id)->where('jaar', $jaar)->firstOrFail();
      return Datatables::of($kommandoJaar->lede)->make(true);
    }
}
