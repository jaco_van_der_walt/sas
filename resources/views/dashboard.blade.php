@extends('layouts.main')

@section('body')
@parent

  <!-- START PAGE CONTENT -->
  <div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
      <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
        <div class="inner">

        <h2>Welkom, {{Auth::user()->name}}</h2>

        </div>
      </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
      <!-- BEGIN PlACE PAGE CONTENT HERE -->
      <div class="panel-body">
      <div class="row">

          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-body">
                <h2>SASv2 is nog in wording!</h2>
                <p>SASv2 is steeds onder ontwikkeling. Dankie vir jou belangstelling</p>
              </div>
          </div>
        </div>
      </div>
    </div>



        </div>
      <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->

@endsection
