<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKommandoJaarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kommando_jare', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('kommando_id');
            $table->smallInteger('jaar')->unsigned();
            $table->timestamps();

            $table->primary('id');
        });

        Schema::table('kommando_jare', function($table) {
           $table->foreign('kommando_id')->references('id')->on('kommandos');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kommando_jare');
    }
}
