<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
class Oord extends Model
{
    use Uuids;
    protected $table = 'oorde';

    public function gebied()
    {
    	return $this->belongsTo('App\Gebied');
    }

    public function kommandos()
    {
    	return $this->hasMany('App\Kommando');
    }

    public $incrementing = false;
}
