<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class OordResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
      return [
        'id' => $this->id,
        'naam' => $this->naam,
        'links' => [
              'self' => url('api/oorde',$this->id),
              'gebied' => url('api/gebiede',$this->gebied_id),
          ],
      ];
    }
}
