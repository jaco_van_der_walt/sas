<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;
use App\Gebied;

class OordeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oorde')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Pionier',
            'gebied_id' => Gebied::where('naam','Transvaal')->firstOrFail()->id,
        ]);

        DB::table('oorde')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Goudrand',
            'gebied_id' => Gebied::where('naam','Transvaal')->firstOrFail()->id,
        ]);

        DB::table('oorde')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Limpopo',
            'gebied_id' => Gebied::where('naam','Transvaal')->firstOrFail()->id,
        ]);

        DB::table('oorde')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Panorama',
            'gebied_id' => Gebied::where('naam','Transvaal')->firstOrFail()->id,
        ]);

        DB::table('oorde')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Moot',
            'gebied_id' => Gebied::where('naam','Transvaal')->firstOrFail()->id,
        ]);

        DB::table('oorde')->insert([
            'id' => Uuid::generate()->string,
            'naam' => 'Monument',
            'gebied_id' => Gebied::where('naam','Transvaal')->firstOrFail()->id,
        ]);
    }
}
