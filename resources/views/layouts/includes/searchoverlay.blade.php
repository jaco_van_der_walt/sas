<!-- START OVERLAY -->
<div class="overlay" style="display: none" data-pages="search">
  <!-- BEGIN Overlay Content !-->
  <div class="overlay-content has-results m-t-20">
    <!-- BEGIN Overlay Header !-->
    <div class="container-fluid">
      <!-- BEGIN Overlay Logo !-->
      <img class="overlay-brand" src="{{asset('html/assets/img/logo.png')}}" alt="logo" data-src="{{asset('html/assets/img/logo.png')}}" data-src-retina="{{asset('html/assets/img/logo_2x.png')}}" width="78" height="22">
      <!-- END Overlay Logo !-->
      <!-- BEGIN Overlay Close !-->
      <a href="#" class="close-icon-light overlay-close text-black fs-16">
        <i class="pg-close"></i>
      </a>
      <!-- END Overlay Close !-->
    </div>
    <!-- END Overlay Header !-->
    <div class="container-fluid">
      <!-- BEGIN Overlay Controls !-->
      <input id="overlay-search" class="no-border overlay-search bg-transparent" placeholder="Soek..." autocomplete="off" spellcheck="false">
      <br>
      <!-- END Overlay Controls !-->
    </div>
    <!-- BEGIN Overlay Search Results, This part is for demo purpose, you can add anything you like !-->
    <div class="container-fluid">
      <div class="search-results m-t-40">
        <p class="bold">Resultate</p>
        <div class="row" id="results_row">

            <!-- Results go here -->

        </div>
      </div>
    </div>
    <!-- END Overlay Search Results !-->
  </div>
  <!-- END Overlay Content !-->
</div>
<!-- END OVERLAY -->
