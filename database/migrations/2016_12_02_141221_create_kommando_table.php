<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKommandoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kommandos', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('naam');
            $table->uuid('oord_id');
            $table->timestamps();

            $table->primary('id');
            $table->foreign('oord_id')->references('id')->on('oorde');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kommandos');
    }
}
