<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oorde', function (Blueprint $table) {
            $table->uuid('id');
            $table->string('naam');
            $table->uuid('gebied_id');
            $table->timestamps();

            $table->primary('id');
            $table->foreign('gebied_id')->references('id')->on('gebiede');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('oorde');
    }
}
