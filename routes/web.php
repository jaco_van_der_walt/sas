<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();
Route::get('/logout', function(){
	Auth::logout();
	return redirect('/');
});

Route::get('/home', 'HomeController@index');
Route::get('/apitoken', 'HomeController@apitoken');
Route::get('/settings', 'HomeController@getSettings');

// Explorer
Route::get('uitsig', 'StruktuurController@getExplorer');


// Gebiede
Route::get('gebiede','GebiedController@getGebiede');
Route::get('gebied/{id}', 'GebiedController@getGebied');

//Oorde
Route::get('oord/{id}', 'OordController@getOord');

//Kommando
Route::get('kommando/{jaar}/{id}', 'KommandoController@getKommando');
Route::get('kommando/{id}', 'KommandoController@getKommandoGuessYear');
Route::get('kommando/lidmaatskap/nuut/{jaar}/{id}', 'KommandoController@getKommandoAddLid');

//Kommandojaar
Route::get('kommandojaar/{jaar}/{id}', 'KommandoController@createKommandoJaar');

//Lede
Route::get('lede', 'LidController@getLede');
Route::get('lededata', 'LidController@anyData');
Route::get('lede/{id}', 'LidController@getLid');
Route::post('lede/soek', 'LidController@searchVRN');

//Datatables
Route::get('datatable/lede', 'DatatablesController@ledeData');
Route::get('datatables/kommando/{id}/jaar/{jaar}/lede', 'DatatablesController@kommandoLedeData');
