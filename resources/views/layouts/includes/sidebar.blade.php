<!-- BEGIN SIDEBAR -->
<div class="page-sidebar" data-pages="sidebar">
  <div id="appMenu" class="sidebar-overlay-slide from-top">
  </div>
  <!-- BEGIN SIDEBAR HEADER -->
  <div class="sidebar-header">
    <img src="{{asset('html/assets/img/logo_white.png')}}" alt="logo" class="brand" data-src="{{asset('html/assets/img/logo_white.png')}}" data-src-retina="{{asset('html/assets/img/logo_white_2x.png')}}" height="22">
    <div class="sidebar-header-controls">
      <button data-toggle-pin="sidebar" class="btn btn-link visible-lg-inline" type="button"><i class="fa fs-12"></i>
      </button>
    </div>
  </div>
  <!-- END SIDEBAR HEADER -->
  <!-- BEGIN SIDEBAR MENU -->
  <div class="sidebar-menu">
    <ul class="menu-items">
      <li class="m-t-30">
        <a href="{{url('gebiede')}}" class="detailed">
          <span class="title">Oorsig</span>
        </a>
        <span class="icon-thumbnail "><i class="fa fa-compass"></i></span>
      </li>
      <li class="">
        <a href="{{url('lede')}}">
          <span class="title">Lede</span>
        </a>
        <span class="icon-thumbnail "><i class="fa fa-users"></i></span>
      </li>
      <li class="">
        <a href="javascript:;">
          <span class="title">Page 3</span>
          <span class=" arrow"></span>
        </a>
        <span class="icon-thumbnail"><i class="pg-grid"></i></span>
        <ul class="sub-menu">
          <li class="">
            <a href="#">Sub Page 1</a>
            <span class="icon-thumbnail">sp</span>
          </li>
          <li class="">
            <a href="#">Sub Page 2</a>
            <span class="icon-thumbnail">sp</span>
          </li>
          <li class="">
            <a href="#">Sub Page 3</a>
            <span class="icon-thumbnail">sp</span>
          </li>
        </ul>
      </li>
    </ul>
    <div class="clearfix"></div>
  </div>
  <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->