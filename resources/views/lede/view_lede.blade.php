@extends('layouts.main')

@section('body')
@parent

  <!-- START PAGE CONTENT -->
  <div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
      <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
        <div class="inner">
          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
            <li><a href="{{url('lede')}}" class="active">Lede</a>
            </li>
          </ul>
          <!-- END BREADCRUMB -->
        </div>
      </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
      <!-- BEGIN PlACE PAGE CONTENT HERE -->

    <table id="tableWithSearch" class="table table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>VRN</th>
                <th>Noemnaam</th>
                <th>Van</th>
                <th>Geboortedatum</th>
                <th>Geslag</th>
            </tr>
        </thead>
    </table>
      <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->

@endsection

@section('plugins')
@parent
<script type="text/javascript" src="{{asset('html/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/datatables-responsive/js/datatables.responsive.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/datatables-responsive/js/lodash.min.js')}}"></script>
<script type="text/javascript" src="{{asset('html/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.min.js')}}"></script>

<script>
$(document).ready(function() {
    $(function() {
      $('#tableWithSearch').DataTable({
          processing: true,
          serverSide: true,
          ajax: "{{url('datatable/lede')}}",
          language: {
            processing: "Verwerking...",
            info: "Wys _START_ tot _END_ van _TOTAL_ opgawes",
            lengthMenu:    "Wys _MENU_ opgawes",
            search: "Soek:",
            paginate: {
              first: "Eerste",
              previous: "Vorige",
              next: "Volgende",
              last: "Laaste"
            },
          },
          columns: [
              { data: 'id', name: 'id', visible: false},
              { data: 'voortrekker_registrasie_nommer', name: 'voortrekker_registrasie_nommer' },
              { data: 'noemnaam', name: 'noemnaam' },
              { data: 'van', name: 'van' },
              { data: 'geboortedatum', name: 'geboortedatum' },
              { data: 'geslag', name: 'geslag'},
          ]
      });

      //Fix the styling
      $('.dataTables_filter input').addClass('form-control');
      var table = $('#tableWithSearch').DataTable();

      // On Row click, navigate to Lid page
      $('#tableWithSearch tbody').on('click', 'tr', function () {
          var data = table.row( this ).data();
          var url = "{{url('lede')}}" + "/" + data.id;
          window.location.href = url;
      } );

      });
});
</script>
@endsection
