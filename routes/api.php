<?php

use Illuminate\Http\Request;
use App\Gebied;
use App\Oord;
use App\Lid;
use App\Http\Resources\GebiedeResource;
use App\Http\Resources\GebiedResource;
use App\Http\Resources\OordeResource;
use App\Http\Resources\OordResource;
use App\Http\Resources\LidResource;
use App\Http\Resources\LedeResource;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->group(function(){
  Route::get('/user', function (Request $request) {
      return $request->user();
  });


  //** Gebiede **//
  Route::get('/gebiede', function()
  {
     return new GebiedeResource(Gebied::all());
  });
  Route::get('/gebiede/{id}', function($id){
    return new GebiedResource(Gebied::find($id));
  });
  Route::post('gebiede', 'GebiedController@postGebiede');
  Route::delete('gebiede/{id}', 'GebiedController@deleteGebiede');
  Route::put('gebiede/{id}', 'GebiedController@updateGebiede');

  //** Oorde **//
  Route::get('/oorde', function(){
    return new OordeResource(Oord::all());
  });
  Route::get('/oorde/{id}', function($id){
    return new OordResource(Oord::find($id));
  });

  //** Persone **//
  Route::get('/lede', function(Request $request){
    $lid = Lid::where('voortrekker_registrasie_nommer', $request->q)->first();
    if($lid)
    {
      return new LidResource($lid);
    }
    else {
      return response()->json(['error'=>'Lid nie gevind nie.'],204);
    }
  });
  Route::get('/lede/{id}', function($id){
    $lid = Lid::find($id);
    if($lid)
    {
      return new LidResource($lid);
    }
    else {
      return response()->json(['error'=>'Lid nie gevind nie.'],204);
    };
  });


  //?q= format
  Route::get('search/lede', function(Request $request){
    Log::info("request called");
    return new LedeResource(App\Lid::search($request->q)->get());
  });

  Route::put('lede/{id}', 'LidController@updateLid');

  Route::post('kommando/lidmaatskap', 'KommandoController@registreerLid');
  Route::get('kommando/{kommando_id}/{jaar}/lede', 'KommandoController@getKommandoLede');

});
