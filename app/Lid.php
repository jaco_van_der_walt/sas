<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App\Traits\Uuids;

class Lid extends Model
{
    use Uuids;
    protected $table = 'lede';

    use Searchable;

    public $incrementing = false;

    public function searchableAs()
    {
        return 'lede';
    }

}
