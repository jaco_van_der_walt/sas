<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class KommandoJaar extends Model
{
    use Uuids;

    protected $table = 'kommando_jare';

    protected $fillable = ['kommando_id', 'jaar']; //Allows Mass Assignment

    public $incrementing = false;

    public function kommando()
    {
    	return $this->belongsTo('App\Kommando');
    }

    public function lede()
    {
      return $this->belongsToMany('App\Lid', 'lidmaatskap', 'kommando_jaar_id', 'lid_id')->withPivot('betrokkenheid_id');
    }

}
