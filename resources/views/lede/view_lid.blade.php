@extends('layouts.main')

@section('body')
@parent

  <!-- START PAGE CONTENT -->
  <div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
      <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
        <div class="inner">
          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
          <li>
            <p><a href="{{url('lede')}}">Lede</a></p>
          </li>
          <li>
            <a class="active" href="{{url()->current()}}">{{$lid->noemnaam}} {{$lid->van}}</a>
          </li>
        </ul><!-- END BREADCRUMB -->
          <!-- END BREADCRUMB -->
        </div>
      </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
      <!-- BEGIN PlACE PAGE CONTENT HERE -->


<div class="panel-body">
    <h1>{{$lid->noemnaam}} {{$lid->van}}</h3>
    <h3>{{$lid->voortrekker_registrasie_nommer}}</h4>
    <br>

    <div class="form-horizontal">
      <div class="form-group">
          <label class="col-sm-3 control-label display-label">Naam</label>
          <div class="col-sm-9">
              {{$lid->noemnaam}}
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-3 control-label display-label">Van</label>
          <div class="col-sm-9">
              {{$lid->van}}
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-3 control-label display-label">Geboortedatum</label>
          <div class="col-sm-9">
              {{$lid->geboortedatum}}
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-3 control-label display-label">Geslag</label>
          <div class="col-sm-9">
              @if($lid->geslag == 'M')
                Manlik
              @else
                Vroulik
              @endif
          </div>
      </div>
    </div>

</div>








      <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
  </div>
  <!-- END PAGE CONTENT -->

@endsection
