<!DOCTYPE html>
<html>
<head>
@include('layouts.includes.head')
</head>
<body class="fixed-header">
@include('layouts.includes.sidebar')

    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
    @include('layouts.includes.header')
        <!-- START PAGE CONTENT WRAPPER -->
        <div class="page-content-wrapper">
        @section('body')
        @show
        @include('layouts.includes.footer')
        </div>
        <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
    @include('layouts.includes.quickview')
    @include('layouts.includes.searchoverlay')
    @section('plugins')
    @include('layouts.includes.plugins')
    @show

</body>
</html>
