<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Kommando extends Model
{
    use Uuids;
    
    protected $table = 'kommandos';

    public function oord()
    {
    	return $this->belongsTo('App\Oord');
    }

    public $incrementing = false;
}
