@extends('layouts.main')

@section('body')
@parent

  <!-- START PAGE CONTENT -->
  <div class="content">
    <!-- START JUMBOTRON -->
    <div class="jumbotron" data-pages="parallax">
      <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
        <div class="inner">
          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
          <li>
            <p><a href="{{url('gebiede')}}">Die Voortrekkers</a></p>
          </li>
          <li>
            <a href="{{url('gebied',[$gebied->id])}}">{{$gebied->naam}}</a>
          </li>
          <li>
            <a href="{{url('oord',[$oord->id])}}">{{$oord->naam}}</a>
          </li>
          <li>
            <a class="active" href="{{url('kommando',[$kommando->id])}}">{{$kommando->naam}}</a>
          </li>
        </ul><!-- END BREADCRUMB -->
        </div>
      </div>
    </div>
    <!-- END JUMBOTRON -->
    <!-- START CONTAINER FLUID -->
    <div class="container-fluid container-fixed-lg">
      <!-- BEGIN PlACE PAGE CONTENT HERE -->

      <div class="panel-body">
        <div class="row">
          <div class="col-md-8 col-sm-6 col-xs-4">
            <h3>{{$kommando->naam}}</h3>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-8">
              <div class="date-container">
                <div class="date-left pull-right"><a href="{{url('kommando',[$jaar +1, $kommando->id])}}"><h4><i class="pg-arrow_lright_line_alt"></i></h4></a></div>
                <div class="date-center pull-right"><a data-toggle="modal" data-target="#myModal"><h3 class="">&nbsp{{$jaar}}&nbsp</h3></a></div>
                <div class="date-right pull-right"><a href="{{url('kommando',[$jaar -1, $kommando->id])}}"><h4><i class="pg-arrow_left_line_alt"></i></h4></a></div>
                <div class="date-clear"></div>
              </div>
          </div>
          </div>

          <div class="text-center">
          <h4>{{$kommando->naam}} het geen data vir {{$jaar}} nie</h4>
          <a href="{{url('kommandojaar',[$jaar, $kommando->id])}}" class="btn btn-primary btn-cons">Skep Data</a>
          </div>
          

        </div>



      <!-- END PLACE PAGE CONTENT HERE -->
    </div>
    <!-- END CONTAINER FLUID -->
    <img class="pull-bottom" src="{{asset('images/tree_landscape_70.svg')}}">
  </div>
  <!-- END PAGE CONTENT -->


  <!-- MODALS -->
  <!-- MODAL STICK UP  -->
<div class="modal fade stick-up" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header clearfix text-left">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="pg-close fs-14"></i>
                </button>
                <h5>Kommando <span class="semi-bold">Jaar</span></h5>
                <p>Kies die jaar wat vertoon moet word</p>
            </div>
            <div class="modal-body">
                <form class="form-default" role="form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <div id="jaar_picker" class="input-group date">
                                    <input id="date_holder" type="text" class="form-control">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-sm-3">
                        <button type="button" id="jaar_button" class="btn btn-primary btn-lg btn-large btn-block" data-dismiss="modal">
                        Opdateer
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- END MODAL STICK UP  -->




@endsection

@section('plugins')
@parent
<!-- Page level scripts -->
<script type="text/javascript" src="{{asset('html/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>

<script>
$(document).ready(function() {
    $("#jaar_picker").datepicker({
    format: "yyyy", // Notice the Extra space at the beginning
    viewMode: "years", 
    minViewMode: "years"
  });

  $( "#jaar_button" ).click(function() {
    //change the year
    var url = "{{url('kommando')}}" + "/" + $("#date_holder").val() + "/{{$kommando->id}}";
    window.location.href = url;
  });


});
</script>
@endsection
